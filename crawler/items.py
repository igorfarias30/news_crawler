# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class CrawlerItem(scrapy.Item):
    company = scrapy.Field()
    title = scrapy.Field()
    link = scrapy.Field()
    datetime = scrapy.Field()

class NewsItem(scrapy.Item):
    texto = scrapy.Field()
    link = scrapy.Field()
