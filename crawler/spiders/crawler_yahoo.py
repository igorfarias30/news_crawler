from scrapy.selector import HtmlXPathSelector
from crawler.items import CrawlerItem
import warnings
import scrapy

warnings.filterwarnings("ignore")

class CrawlerYahoo(scrapy.Spider):

    name = 'yahoo'
    custom_settings = {
        'ITEM_PIPELINES':{
            'crawler.pipelines.StoreYahooMongoDB': 500
        }
    }

    organizations = ['FB', 'VALE', 'MSFT'] # company to crawler
    company = None

    #making the url
    def get_url(self, company):
        return f"https://finance.yahoo.com/quote/{company}/news?p=FB"
        #return f"https://finance.yahoo.com/quote/{company}?p={company}&.tsrc=fin-srcha"

    def start_requests(self):
        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0'}
        for org in self.organizations:
            url = self.get_url(org)
            yield scrapy.Request(url, callback = self.parse, headers = headers)

    def parse(self, response):
        # get the company name in url
        def getCompanyUrl(url):
            for company in self.organizations:
                 if company in url.split('?')[0].split('/'):
                     return company.lower()
            return None

        page = HtmlXPathSelector(response)
        #id = "quoteNewsStream-0-Stream"
        id = "latestQuoteNewsStream-0-Stream"
        links = page.xpath(f'//div[@id="YDC-Col1"]//div[@id="{id}"]//ul//li//h3//a//@href').getall()
        titles = page.xpath(f'//div[@id="YDC-Col1"]//div[@id="{id}"]//ul//li//h3//a//text()').getall()
        texts = page.xpath(f'//div[@id="YDC-Col1"]//div[@id="{id}"]//ul//li//p//text()').getall()

        for link, title, text in zip(links, titles, texts):
            item = CrawlerItem()
            item["company"] = getCompanyUrl(response.url)
            item["title"] = title
            item["link"] = link
            item["datetime"] = text
            yield item
