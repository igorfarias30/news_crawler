from crawler.items import CrawlerItem
import warnings
import datetime
import pymongo # connection = pymongo.MongoClient('localhost', 27017)["stock_news"]['nasdaq']
import scrapy

warnings.filterwarnings("ignore")

class NasdaqCrwaler(scrapy.Spider):

    name = 'nasdaq'
    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.StoreNewsMongoDB': 300
        }
    }

    organizations = ['fb', 'vale', 'msft'] # company to crawler
    company = None

    # making the url
    def get_url(self, org):
        return f"https://www.nasdaq.com/symbol/{org}/news-headlines"

    # function to make the request for each company
    def start_requests(self):

        for org in self.organizations:
            self.company = org

            if org == 'vale':
                url = self.get_url(org);
            elif org == 'msft':
                url = self.get_url(org);
            elif org == 'fb':
                url = self.get_url(org);
            else:
                pass

            yield scrapy.Request(url, self.parse)

    def parse(self, response):

        # get the news date in this format - ''
        def getDatetime(date):
            date = date.split('-')[0].split()[1]
            month, day, year = date.split('/')
            return datetime.datetime(int(year), int(month), int(day))\
                            .isoformat()\
                            .split('T')[0]

        # get the company name in url
        def getCompanyUrl(url):
            for company in self.organizations:
                 if company in url.split('/'):
                     return company
            return None

        for link, small in zip(response.css('div.news-headlines div span.fontS14px'),  response.css('div.news-headlines div small')):

            item = CrawlerItem()
            item['company'] = getCompanyUrl(response.url)
            item['title'] = link.css('a::text').get(default = '')
            item['link'] = link.css('a::attr("href")').get()
            item['datetime'] = getDatetime(small.get())

            yield item

        for next_page in response.css('ul.pager li a::attr("href")'):
            yield response.follow(next_page, self.parse)
