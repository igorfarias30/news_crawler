from scrapy.selector import HtmlXPathSelector
from crawler.items import NewsItem
from scrapy.conf import settings
import warnings
import datetime
import pymongo
import scrapy

warnings.filterwarnings("ignore")

class ContextCrawler(scrapy.Spider):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]

    name = 'context'
    organizations = ['vale', 'msft', 'fb']

    custom_settings = {
        'ITEM_PIPELINES': {
            'crawler.pipelines.NewsMongoDB': 400
        }
    }

    def start_requests(self):
        # interate in organizations actives

        headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:48.0) Gecko/20100101 Firefox/48.0'}

        for org in self.organizations:
            query = {"company": org}

            for data in self.collection.find(query):
                if 'nasdaq' in data['link'].split('.'):
                    yield scrapy.Request(data['link'], callback = self.parseNasdaq, headers = headers, meta = {"url": data['link']})
                elif 'investopedia' in data['link'].split('.'):
                    yield scrapy.Request(data['link'], callback = self.parseInvestopedia, headers = headers, meta = {"url": data['link']})
                if 'https://seekingalpha' in data['link'].split('.'):
                    yield scrapy.Request(data['link'], callback = self.parseSeekingAlpha, headers = headers, meta = {"url": data['link']})
                else:
                    pass

    # getting context in nasdaq's website
    def parseNasdaq(self, response):
        hxs = HtmlXPathSelector(response)
        text = hxs.xpath('//div[@id="container"]') # [@id="articleText"]

        item = NewsItem()
        item['texto'] = ' '.join(text.xpath('//p/text()').getall())
        item['link'] = response.url
        yield item

    # getting context in investopedia's website
    def parseInvestopedia(self, response):
        hxs = HtmlXPathSelector(response)
        text = hxs.xpath('//div[@id="article-body_1-0"]/div') # [@id="articleText"]

        item = NewsItem()
        item['texto'] = ' '.join(text.xpath('//p/text()').getall())
        item['link'] = response.meta['url']
        yield item

    # getting context in seekingalpha's website
    def parseSeekingAlpha(self, response):
        hxs = HtmlXPathSelector(response)
        text = hxs.xpath('//div[@class="container"]')

        item = NewsItem()
        item['texto'] = ' '.join(text.xpath('//p/text()').getall())
        item['link'] = response.meta['url']
        yield item
