# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

from scrapy.exceptions import DropItem
from scrapy.conf import settings
from scrapy import log
import pymongo

class StoreNewsMongoDB(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]

    # getting each news crawled
    def process_item(self, item, spider):
        for data in item:
            if not data:
                raise DropItem("Missing data!")

        self.collection.insert({"company": item['company'],
                                "title": item['title'],
                                "link": item['link'],
                                "datetime": item["datetime"]})

        log.msg("News added to stock_news.nasdaq", level=log.DEBUG, spider=spider)
        return item

# getting the context and persisting in mongodb
class NewsMongoDB(object):

    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION']]

    # getting each news crawled
    def process_item(self, item, spider):
        for data in item:
            if not data:
                raise DropItem("Missing data!")

        item = dict(item)
        query = {"link": item['link']}

        self.collection.update_one(query, {"$set": {
                                                    "context": item['texto']
                                                    }
                                            }, upsert=True)

        log.msg("Context added to stock_news.nasdaq", level=log.DEBUG, spider=spider)
        return item

# Yahoo Finance
class StoreYahooMongoDB(object):
    def __init__(self):
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = connection[settings['MONGODB_DB']]
        self.collection = db[settings['MONGODB_COLLECTION_YAHOO']]

    def process_item(self, item, spider):
        self.collection.update({"link": item['link']}, dict(item), upsert = True)
        log.msg("Yahoo links added to Mongo in collection 'Yahoo'", level = log.DEBUG, spider = spider)
        return item
